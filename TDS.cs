﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Compiler
{
    public class TDS
    {
        /// <summary>
        /// clave primaria
        /// </summary>
        private int tds_id;
        /// <summary>
        /// tipo de variable
        /// </summary>
        private int tds_type;
        /// <summary>
        /// valor de la variable
        /// </summary>
        private string tds_value;
        /// <summary>
        /// nombre del identificador
        /// </summary>
        private string tds_var;
        /// <summary>
        /// tamaño de la variable
        /// </summary>
        private int tds_size;

        public TDS()
        {
            throw new System.NotImplementedException();
        }

        public int get_tds_id()
        {
            throw new System.NotImplementedException();
        }

        public int get_tds_type()
        {
            throw new System.NotImplementedException();
        }
    }
}