﻿//      *********    NO MODIFIQUE ESTE ARCHIVO     *********
//      Este archivo se regenera mediante una herramienta de diseño.
//       Si realiza cambios en este archivo, puede causar errores.
namespace Expression.Blend.SampleData.GramaticaSampleDataSource
{
    using System; 
    using System.ComponentModel;

// Para reducir de forma significativa la superficie de los datos de ejemplo en la aplicación de producción, puede establecer
// la constante de compilación condicional DISABLE_SAMPLE_DATA y deshabilitar los datos de ejemplo en tiempo de ejecución.
#if DISABLE_SAMPLE_DATA
    internal class gramatica { }
#else

    public class gramatica : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public gramatica()
        {
            try
            {
                Uri resourceUri = new Uri("ms-appx:/SampleData/GramaticaSampleDataSource/GramaticaSampleDataSource.xaml", UriKind.RelativeOrAbsolute);
                System.Windows.Application.LoadComponent(this, resourceUri);
            }
            catch
            {
            }
        }

        private tokenCollection _alfabeto = new tokenCollection();

        public tokenCollection alfabeto
        {
            get
            {
                return this._alfabeto;
            }
        }

        private no_terminalesCollection _no_terminalesCollection = new no_terminalesCollection();

        public no_terminalesCollection no_terminalesCollection
        {
            get
            {
                return this._no_terminalesCollection;
            }
        }

        private reglaCollection _reglas = new reglaCollection();

        public reglaCollection reglas
        {
            get
            {
                return this._reglas;
            }
        }
    }

    public class token : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string _lexema = string.Empty;

        public string lexema
        {
            get
            {
                return this._lexema;
            }

            set
            {
                if (this._lexema != value)
                {
                    this._lexema = value;
                    this.OnPropertyChanged("lexema");
                }
            }
        }

        private double _n_token = 0;

        public double n_token
        {
            get
            {
                return this._n_token;
            }

            set
            {
                if (this._n_token != value)
                {
                    this._n_token = value;
                    this.OnPropertyChanged("n_token");
                }
            }
        }

        private string _sinonimo = string.Empty;

        public string sinonimo
        {
            get
            {
                return this._sinonimo;
            }

            set
            {
                if (this._sinonimo != value)
                {
                    this._sinonimo = value;
                    this.OnPropertyChanged("sinonimo");
                }
            }
        }

        private string _nombretk = string.Empty;

        public string nombretk
        {
            get
            {
                return this._nombretk;
            }

            set
            {
                if (this._nombretk != value)
                {
                    this._nombretk = value;
                    this.OnPropertyChanged("nombretk");
                }
            }
        }
    }

    public class no_terminales : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string _nombre = string.Empty;

        public string nombre
        {
            get
            {
                return this._nombre;
            }

            set
            {
                if (this._nombre != value)
                {
                    this._nombre = value;
                    this.OnPropertyChanged("nombre");
                }
            }
        }

        private string _sinonimo = string.Empty;

        public string sinonimo
        {
            get
            {
                return this._sinonimo;
            }

            set
            {
                if (this._sinonimo != value)
                {
                    this._sinonimo = value;
                    this.OnPropertyChanged("sinonimo");
                }
            }
        }
    }

    public class regla : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string _p_izquierda = string.Empty;

        public string p_izquierda
        {
            get
            {
                return this._p_izquierda;
            }

            set
            {
                if (this._p_izquierda != value)
                {
                    this._p_izquierda = value;
                    this.OnPropertyChanged("p_izquierda");
                }
            }
        }

        private string _p_derecha = string.Empty;

        public string p_derecha
        {
            get
            {
                return this._p_derecha;
            }

            set
            {
                if (this._p_derecha != value)
                {
                    this._p_derecha = value;
                    this.OnPropertyChanged("p_derecha");
                }
            }
        }
    }

    public class tokenCollection : System.Collections.ObjectModel.ObservableCollection<token>
    { 
    }

    public class no_terminalesCollection : System.Collections.ObjectModel.ObservableCollection<no_terminales>
    { 
    }

    public class reglaCollection : System.Collections.ObjectModel.ObservableCollection<regla>
    { 
    }
#endif
}
