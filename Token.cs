﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Compiler
{
    public class Token
    {
        /// <summary>
        /// guarda numero de token reconocido
        /// </summary>
        private int numtoken;
        /// <summary>
        /// terminal de la gramàtica
        /// </summary>
        private string sinonimo;
        /// <summary>
        /// lo que reconoce
        /// </summary>
        private string lexema;
        /// <summary>
        /// nombre del token
        /// </summary>
        private string nombretk;

        /// <summary>
        /// Devuelve el numero de token
        /// </summary>
        public int devuelve_num_token()
        {
            return numtoken;
        }

        /// <summary>
        /// Devuelve el simbolo terminal
        /// </summary>
        public string devuelve_sinonimo()
        {
            return sinonimo;
        }

        /// <summary>
        /// Devuleve el lexema
        /// </summary>
        public string devuelve_lexema()
        {
            return lexema;
        }

        /// <summary>
        /// Devueleve nombre token
        /// </summary>
        public string devuelve_nombtk()
        {
            return nombretk;
        }

        public void set_lexema (string lex)
        {
            this.lexema = lex;

        }

        public Token(int ntk, string sintk, string lextk, string nombtk)
        {
            numtoken = ntk;
            sinonimo = sintk;
            lexema = lextk;
            nombretk = nombtk; ;
        }

        public Token()
        {
            numtoken = 0;
            sinonimo = null;
            lexema = null;
            nombretk = null;
        }
    }
}