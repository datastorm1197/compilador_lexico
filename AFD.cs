﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Compiler
{
    
    public class AFD
    {
        public int[,] elementos;
        public string columnas;
        public string[] datos;

        public AFD()
        {
            this.elementos = null;
            //this.columnas = "abcdefghijklmnopqrstuvwxyz0123456789]+*-|();.'#/^!<>=:,@";
            this.columnas = null;
            this.datos = null;

        }

        private int getpos_columna(char simbolo)
        {
            if (simbolo == '"')
                simbolo = ']';
            if (simbolo == ' ')
                simbolo = '@';
            if (simbolo == ';')
                simbolo = '?';
            int numsimbolos = this.columnas.Length;
            for (int i=0; i<numsimbolos; i++)
            {
                if (this.columnas[i] == simbolo)
                    return i;
            }
            return -1;
        }
        public int getEstado (int fila, char simbolo)
        {
            return this.elementos[fila, getpos_columna(simbolo)];
        }

    }


}
