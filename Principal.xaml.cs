﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace Compiler
{
    /// <summary>
    /// Lógica de interacción para Principal.xaml
    /// </summary>
    public partial class Principal : Window
    {
        public AFD matrizAFD = new AFD();
        List<Token> listTK = new List<Token>();
        List<Token> tokensReconocidos = new List<Token>();
             
        public Principal()
        {
            InitializeComponent();
            
        }

        public string cargar_archivo(string archivo)
        {
            string cad = "";
            try
            {
                using (StreamReader sr = new StreamReader(archivo))
                {
                    cad = sr.ReadToEnd();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("El archivo no se pudo cargar ó \nno se ha escogido ningun archivo", "Error");
            }
            return cad;
        }

        public void Crearmatrizlex(string archivodevuelto)
        {
            matrizAFD.datos = archivodevuelto.Split('%');
            //MostrarMat.Document.Text = datos[0];
            string[] mat = matrizAFD.datos[0].Split('\n');
            //pos_tokns = matrizAFD.datos[2].Split('\t');
            int filas = mat.Length - 1;
            string[] tokns = matrizAFD.datos[1].Split('\t');
            int col = tokns.Length;
            matrizAFD.elementos = new int[filas, col];
            int valor;
            for (int i = 0; i < filas; i++)
            {
                //LSTMATLEX1.Items.Add(i.ToString() + "\t" + mat[i]);
                string[] aux = mat[i].Split('\t');
                for (int j = 0; j < col; j++)
                {
                    valor = Int32.Parse(aux[j]);
                    if (valor != 900)
                        matrizAFD.elementos[i, j] = valor;
                }
            }
            char c;
            for (int k = 0; k < col-1; k++)
            {
                c = char.Parse (tokns[k]);
                matrizAFD.columnas += c;
            }
            matrizAFD.columnas += '@';
        }

        private void mniCargarAFD_click(object sender, RoutedEventArgs e)
        {
            //..\\..\\Archivos\\MatrizLexica.txt
            // Cargar el Automata Finito Determinista desde archivo a la matriz de transiciones
            Crearmatrizlex(cargar_archivo("MatrizLexica.txt"));

            // Poner los lexemas en la lista de Tokens listTK 
            Cargar_Alfabeto_en_Tokens();
        }

        // sube del archivo alfabeto.txt a unalista de tokens listTK
        public void Cargar_Alfabeto_en_Tokens()
        {
            string lexema = cargar_archivo("alfabeto.txt");
            string[] vec_lex = lexema.Split('\n');
            for (int i = 0; i < vec_lex.Length; i++)
            {
                string[] aux1 = vec_lex[i].Split('\t');
                Token t = new Token(Int32.Parse(aux1[0]), aux1[1], aux1[2], aux1[3]);
                listTK.Add(t);
            }
        }

        // de la lista de tokens devuelvre el Token almacenado
        public Token devuelve_reco(int valor)
        {
            Token vacio = new Token();
            for (int i = 0; i < listTK.Count; i++)
            {
                if (listTK[i].devuelve_num_token() == valor)
                {
                    return listTK[i];
                }
            }
            return vacio;
        }

        private void mniScanner_click(object sender, RoutedEventArgs e)
        {
            // leer un archivo texto que serà archivo fuente
            string archivo_fuente = cargar_archivo("Fuente.txt");
            // Algoritmo del lexico visto en clase
            // se debe leer el archivo fuente por lìneas y luego por palabras
            // cada palabra entra a reconocer
            // int estado=0;
            //  string lexema = null;
            // while (estado > 0) 
            // {
            //        simbolo = palabra[i]
            //        lexema += simbolo;
            //        ëstado=matrizAFD.getEstado (estado, simbolo)
            //}
            // Token tktemporal = devuelve_reco(-estado);
            // tktemporal.setlexema (lexema);
            // tokensReconocidos.add(tktempooral);
            // 
            mn
        }
    }



}


